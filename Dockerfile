# Bug-dedup base
FROM ubuntu:16.04

MAINTAINER Michelle Naylor mnaylor@ualberta.ca

RUN apt-get update && apt-get install -y \
    build-essential \
    python \
    python-dev \
    python-pip \
    git \
    uuid-dev \
    sqlite3 \
    libsqlite3-dev \
    libtool \
    automake \
    libboost-all-dev \
    logrotate \
    rsyslog \
    cmake || echo "Ignoring poor return value"
# install java
RUN apt-get install -y \
    openjdk-8-jdk \
    openjdk-8-jre

#install perl and python
RUN apt-get install -y \
    cpanminus \
    r-base \
    littler \
    wget \
    python-numpy \
    python-scipy

# Fix R LOCALE errors
ENV LANG "en_US.UTF-8"

# SSH key stuff
RUN mkdir /root/.ssh
ADD id_rsa /root/.ssh/id_rsa
RUN touch /root/.ssh/known_hosts
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts
RUN apt-get install -y locales
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# install perl modules
RUN cpanm -f Heap::Simple::XS

# python deps
RUN pip install nltk python-dateutil pyinotify sphinx

RUN cd && mkdir logs

# setup vowpal_wabbit - checkout version 8.2.0
RUN cd && git clone https://github.com/JohnLangford/vowpal_wabbit.git && \
    cd vowpal_wabbit && \
    git checkout tags/8.2.0 && \
    autoreconf -vfi && \
    ./configure --with-boost-libdir=/usr/lib/x86_64-linux-gnu && \
    make -j 8 && make install

# setup flann
RUN git clone https://github.com/mariusmuja/flann.git && \
    cd flann && cmake ./ && make && make install

# RUN apt-get -y install libsodium-dev libsodium
# setup sodium for zeromq
RUN cd && wget https://download.libsodium.org/libsodium/releases/libsodium-1.0.15.tar.gz
RUN cd && tar -xzvf libsodium-1.0.15.tar.gz && cd libsodium-1.0.15 && \
    ./configure && make && make install && cd
RUN ldconfig

RUN apt-get -y install apt-utils
RUN apt-get -y install pkg-config

# setup zeromq
# was 2.1.4
RUN wget http://download.zeromq.org/zeromq-4.1.4.tar.gz
RUN tar -xzvf zeromq-4.1.4.tar.gz
RUN cd zeromq-4.1.4/ && ./configure && make && make install && ldconfig
# setup zeromq

RUN pip install pyzmq

# RUN apt-get -y install libzmq1 libzmq-dev

# setup mongrel2
RUN cd && git clone https://github.com/mongrel2/mongrel2.git
RUN cd && cd mongrel2 && make all install && \
    cd examples/python && python setup.py install

# run cron for logging
ADD logrotate_conf /etc/logrotate.d/bugparty
RUN chmod 644 /etc/logrotate.d/bugparty

ENV LOGNAME docker
