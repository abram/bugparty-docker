#!/bin/bash

# install nsenter if we don't have it yet
if [ -z `which nsenter` ]; then
	sudo docker run -v /usr/local/bin:/target jpetazzo/nsenter
fi


username=yourpalal # used for default names below

database=elasticsearch # set to couchdb to use couchdb

couchdb_image="$username"/couchdb:bugparty
couchdb_name=couchdb

es_image="$username"/es:bugparty
es_name=es

bugparty_image="$username"/bug-dedup:$database
bugparty_name=bugparty

case $database in
	$couchdb_name )
		database_name=$couchdb_name
		database_image=$couchdb_image
		bugparty_folder=bug-dedup-couch
	;;
	$es_name | elasticsearch )
		database_name=$es_name
		database_image=$es_image
		bugparty_folder=bug-dedup-es
	;;
	* )
		echo unrecognized database name "'$database'"
		exit -10000
	;;
esac

data_image="$username"/bugparty_data
data_name=data

source_host_dir=~/projects/bugparty/
# this will be mounted as /src when the bugparty container is run
# bugparty source should be in there at ~/projects/bugparty/bugparty


couchdb_volume_location=~/couchdb
es_volume_location=~/bugparty/es_data


function get_bugparty_pid() {
	# this code based on docker-enter as provided by jpetazzo/nsenter
	PID=$(docker inspect --format {{.State.Pid}} "$bugparty_name")
	if [ -z "$PID" ]; then
		exit 1
	fi
	echo $PID
}

function up() {
	for image in $@ ; do
		case $image in
		$es_name )
            docker network create bp-net
			docker run -d -p 127.0.0.1:9200:9200 -p 127.0.0.1:9300:9300  --name $es_name \
				--net bp-net \
				--log-opt max-size=50m  \
				-v $es_volume_location:/usr/share/elasticsearch/data/ \
				$es_image elasticsearch start
		;;
		$couchdb_name )
			docker run -d -p 127.0.0.1:5984:5984 -p 127.0.0.1:5985:5985  --name $couchdb_name  \
				-v $couchdb_volume_location:/usr/local/var/lib/couchdb \
				$couchdb_image
		;;
		$data_name )
			docker run -d -i --name $data_name $data_image \
				--log-opt max-size=50m  \
				/bin/bash
		;;
		esac
	done
}


function build () {
	for image in $@ ; do
		echo building $image

		case $image in
		base )
			# Make id_rsa if it isn't here
			if [ ! -e "id_rsa" ]; then
				echo no default id_rsa provided, make our own!
				ssh-keygen -f id_rsa -q -N ""
			fi
			if [ ! -e $source_host_dir ]; then
				mkdir -p $source_host_dir
				git clone https://bitbucket.org/abram/bugparty -b mvel $source_host_dir/bugparty
				mkdir -p $source_host_dir/bug-deduplication
				git clone https://bitbucket.org/abram/bp-bug-deduplication.git -b elasticsearch --single-branch $source_host_dir/bug-deduplication
				git clone https://bitbucket.org/abram/fastrep.git --branch elasticsearch --single-branch $source_host_dir/fastrep
			fi
			if [ ! -e $es_volume_location ]; then
				mkdir -p $es_volume_location
			fi
			docker build --rm -t mnaylor/base . || \
				(echo "Could not build base"; exit 1)
		;;
		$es_name )
			docker build --rm -t $es_image elasticsearch || \
				(echo "Could not build elasticsearch $es_name"; exit 1)
		;;
		$couchdb_name )
			docker build --rm -t $couchdb_image couchdb || \
				(echo "Could not build couchdb $couchdb_image"; exit 1)
		;;
		$bugparty_name )
			echo building $bugparty_image
			docker build --rm -t $bugparty_image $bugparty_folder || \
				(echo "Could not build bugparty $bugparty_name"; exit 1)
		;;
		$data_name )
			docker build --rm -t $data_image data || \
				(echo "Could not build data $data_name"; exit 1)
		;;
		esac
	done
}


command=$1
shift
case $command in
	down )
		docker kill $database_name $data_name
		if [ $# -gt 0 -a "$1" == rm ]; then
			docker rm $database_name $data_name
		fi
	;;

	up )
		if [ $# -eq 0 ] ; then
			up $database_name $data_name
			docker start $database_name $data_name
		else
			up $@
			docker start $@
		fi
	;;

	shell ) # open a shell on $bugparty_name with an environment somewhat
		# like the one the server gets

		# /init_shell is used as the init script so that the run_server_command
		# can share environment variables with the shell
		sudo nsenter --target `get_bugparty_pid` --mount --uts --ipc --net \
			--pid -- /bin/bash -rcfile /init_shell
		reset
	;;

	run ) # run a comand on $bugparty_name (like shell, but just one command)
		command_as_string="$@"
		sudo nsenter --target `get_bugparty_pid` --mount --uts --ipc --net \
			--pid -- /bin/bash -rcfile /init_shell -i -c "$command_as_string"
	;;

	serve ) # run the server
		ini_file=/etc/bugparty/bugparty.ini
		if [ $# -gt 0 -a "$1" == 'tests' ] ; then
			ini_file=/etc/bugparty/bugparty-testing.ini
		fi
		docker rm $bugparty_name
                curl -XPUT http://127.0.0.1:9200/bugparty/
		#docker run --rm --name "$bugparty_name" -t -i \
		#	-p 127.0.0.1:8080:8080 \
		#    --link $database_name:$database_name \
		#	-v "$source_host_dir":/src --volumes-from "$data_name" \
		#   	--workdir /var/lib/bugparty "$bugparty_image" \
		#   		/bin/bash /etc/bugparty/run_dev_server /src/bugparty $ini_file

		docker run --rm --name "$bugparty_name" -t -i \
			-p 127.0.0.1:8080:8080 \
                        --net bp-net \
		        --link $database_name:$database_name \
			-v "$source_host_dir":/src --volumes-from "$data_name" \
		   	--workdir /var/lib/bugparty "$bugparty_image" \
		   		/bin/bash /etc/bugparty/run_dev_server /src/bugparty $ini_file
		    #--link $database_name:$database_name \
                    #
		    # --net=host \
                    #

	;;
	headless ) # run the server
		ini_file=/etc/bugparty/bugparty.ini
		if [ $# -gt 0 -a "$1" == 'tests' ] ; then
			ini_file=/etc/bugparty/bugparty-testing.ini
		fi
		docker rm $bugparty_name
                curl -XPUT http://127.0.0.1:9200/bugparty/

		docker run -d --name "$bugparty_name" -i \
			-p 127.0.0.1:8080:8080 \
                        --net bp-net \
						--log-opt max-size=50m  \
		        --link $database_name:$database_name \
			-v "$source_host_dir":/src --volumes-from "$data_name" \
		   	--workdir /var/lib/bugparty "$bugparty_image" \
		   		/bin/bash /etc/bugparty/run_dev_server /src/bugparty $ini_file

	;;



	build ) # build an image
		if [ $# -eq 0 ] ; then
			build base $database_name $data_name $bugparty_name
		else
			build $@
		fi
	;;

	* )
		echo "command '$command' not recognized"
	;;
esac
