Bug Party Bug Deduplication System
==================================

Welcome to BugParty/DupeBuster/Düpbuster Bug deduplication system.

This is a webservice that offers you the ability to mirror and
query for duplicate bug reports.

Using Devenv
====================

The devenv script provides an easy-to-use wrapper that automates many
common docker tasks related to this repo. Before using it, you should
set the username and database variables near the top of the script.
There are a number of other variables you might want to change, all
appearing near the top of the script.

Once the script has been modified to your liking, you can run

		./devenv build

To build all of the docker images you need. (This takes a while)
Once the docker images have been built,

		./devenv up

will bring up the development environment.


		./devenv serve tests

will start the webserver in a docker container, connected to the other
containers started with devenv up (using the test config). This will run
front\_controller using util/dev\_server.py by default, so it will
be automatically restarted when you change a file.

		./devenv shell

will launch a shell in the docker container that is running the server, and

		./devenv run <some command>

will run commands in the container that is running the server.

Both the shell and run commands start in /src/bugparty, which is the mounted
copy of bugparty from your host machine. By default, this folder is
equivalent to ~/bugparty on your host, but you can change which folder is
mounted for development in devenv.


To run the server with the non-test config, you can run

		./devenv serve

Build Requirements
=====================
The dockerfiles required to deploy bug-deduplication and bugparty

your private ssh key (id\_rsa) should be copied to this directory


Build Containers
================

* Just use devenv. Make sure you have docker permissions. It should build the containers

        ./devenv build



Run Containers
==============

Elasticsearch
-------------
1. run elasticsearch instance and data volume

        ./devenv up

2. run bug-dedup and link it to couchdb and data containers
        ./devenv serve

Troubleshooting
---------------------

* Did the build die at add id_rsa ? 
  * Well run ssh-keygen and copy the id_rsa and id_rsa.pub you like and put it into the docker directory that you cloned. It will add those instead. Don't share any keys you don't want to share.
  * You should make a key pair before you start in your docker/ dir
     * ssh-keygen -t rsa -N "" -f ./id_rsa
  * Make that id_rsa.pub a bitbucket account just in case
* Did the git repos not clone properly from the devenv build?
  * go and change the docker files from s#git@bitbucket.org:#http://bitbucket.org/# -- use the public repos and you won't have problem
* I still can't clone bugparty! It says it is private.
  * Change it to bugparty-pub instead
* I need Elasticsearch globally visible
  * Edit the elasticsearch.yml file and add
    network.bind_host: "0.0.0.0"
  * docker rm es (after you devenv'd down everything)
* I use SE Linux
  * Be sure to export your homedirectory:
    sudo chcon -Rt svirt_sandbox_file_t /yourhomedir
* Do you have fastrep, bug-deduplication, docker and bugparty all checked out in your homedir?
* Is it whining about projects? 
  * Did it install the right version of python's elasticsearch library?
* Is it whining about projects? 
  * Make the bug party index
    curl -XPUT http://127.0.0.1:9200/bugparty/
